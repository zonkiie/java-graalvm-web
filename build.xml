<project name="JavaGraalVMWeb" basedir="." default="compile">

    <property name="src.dir"     value="src"/>

    <property name="build.dir"   value="build"/>
    <property name="classes.dir" value="${build.dir}/classes"/>
    <property name="jar.dir"     value="${build.dir}/jar"/>

    <property name="main-class"  value="JavaGraalVMWeb.Main"/>
    <property name="lib.dir"     value="lib"/>
    <property name="tmp.dir"     value="/dev/shm/ant_java_graalvm_web"/>
    <property name="hibernate.release" value="5.4.30.Final"/>
    <property name="hibernate.dl.location"         value="https://sourceforge.net/projects/hibernate/files/hibernate-orm/${hibernate.release}/hibernate-release-${hibernate.release}.zip/download"/>
    <property name="hibernate.dl.localfile"  value="hibernate-release-${hibernate.release}.zip"/>
    <property name="h2.release" value="2019-10-14"/>
    <property name="h2.dl.location" value="https://h2database.com/h2-${h2.release}.zip" />
    <property name="h2.dl.localfile" value="h2-${h2.release}.zip" />
    <property name="hsqldb.release" value="2.6.0" />
    <property name="hsqldb.dl.location" value="https://sourceforge.net/projects/hsqldb/files/hsqldb/hsqldb_2_6/hsqldb-${hsqldb.release}.zip/download" />
    <property name="hsqldb.dl.localfile" value="hsqldb-${hsqldb.release}.zip" />
    <property name="slf4j.dl.location" value="https://repo1.maven.org/maven2/org/slf4j/slf4j-simple/1.7.25/slf4j-simple-1.7.25.jar" />
    <property name="slf4j.dl.localfile" value="slf4j-simple-1.7.25.jar" />
    <property name="quarkuscore_deployment.dl.location" value="https://repo1.maven.org/maven2/io/quarkus/quarkus-core-deployment/1.13.2.Final/quarkus-core-deployment-1.13.2.Final.jar" />
    <property name="quarkuscore_deployment.dl.localfile" value="quarkus-core-deployment-1.13.2.Final.jar" />
    <property name="quarkuscore.dl.location" value="https://repo1.maven.org/maven2/io/quarkus/quarkus-core/1.9.2.Final/quarkus-core-1.9.2.Final.jar" />
    <property name="quarkuscore.dl.localfile" value="quarkus-core-1.9.2.Final.jar" />
    <property name="javac.path" value="/usr/lib/jvm/graalvm-ce-java16/bin/javac" />
    <property name="native_image.path" value="/usr/lib/jvm/graalvm-ce-java16/bin/native-image" />
    <property name="jarfile" value="build/JavaGraalVMWeb.jar" />

    <path id="classpath">
        <fileset dir="${lib.dir}" includes="**/*.jar"/>
    </path>

    <target name="clean">
        <delete dir="${build.dir}"/>
        <delete dir="${lib.dir}"/>
        <delete dir="${tmp.dir}"/>
    </target>

    <target name="fetch_libraries">
        <mkdir dir="${tmp.dir}"/>
        <get src="${hibernate.dl.location}" dest="${tmp.dir}/${hibernate.dl.localfile}" skipexisting="true" />
        <!-- <get src="${h2.dl.location}" dest="${tmp.dir}/${h2.dl.localfile}" skipexisting="true"/> -->
        <get src="${hsqldb.dl.location}" dest="${tmp.dir}/${hsqldb.dl.localfile}" skipexisting="true" />
        <get src="${slf4j.dl.location}" dest="${tmp.dir}/${slf4j.dl.localfile}" skipexisting="true" />
        <get src="${quarkuscore.dl.location}" dest="${tmp.dir}/${quarkuscore.dl.localfile}" skipexisting="true" />
        <get src="${quarkuscore_deployment.dl.location}" dest="${tmp.dir}/${quarkuscore_deployment.dl.localfile}" skipexisting="true" />
    </target>

    <target name="unpack_libraries" depends="fetch_libraries">
        <delete dir="${lib.dir}"/>
        <mkdir dir="${lib.dir}"/>
        <unzip src="${tmp.dir}/${hibernate.dl.localfile}" dest="${lib.dir}" overwrite="true"/>
        <!-- <unzip src="${tmp.dir}/${h2.dl.localfile}" dest="${lib.dir}" overwrite="true" /> -->
        <unzip src="${tmp.dir}/${hsqldb.dl.localfile}" dest="${lib.dir}" overwrite="true" />
        <exec dir="${lib.dir}" executable="/usr/bin/find">
            <arg value="-type"/>
            <arg value="f"/>
            <arg value="-iname"/>
            <arg value="*.jar"/>
            <arg value="-exec"/>
            <arg value="cp"/>
            <arg value="-a"/>
            <arg value="{}"/>
            <arg value="."/>
            <arg value=";"/>
        </exec>
        <copy file="${tmp.dir}/${slf4j.dl.localfile}" todir="${lib.dir}" overwrite="true" />
        <copy file="${tmp.dir}/${quarkuscore.dl.localfile}" todir="${lib.dir}" overwrite="true" />
        <copy file="${tmp.dir}/${quarkuscore_deployment.dl.localfile}" todir="${lib.dir}" overwrite="true" />
        <delete dir="${lib.dir}/hibernate-release-${hibernate.release}"/>
        <delete dir="${lib.dir}/h2" />
        <delete dir="${lib.dir}/hsqldb-${hsqldb.release}"/>
    </target>

    <target name="libmerge" depends="unpack_libraries">
        <delete file="${lib.dir}/libs.jar" quiet="true" failonerror="false"/>
        <exec dir="${lib.dir}" executable="/usr/bin/zipmerge">
            <arg value="${lib.dir}/libs.jar" />
            <arg value="*.jar" />
        </exec>
    </target>

    <target name="compile" depends="unpack_libraries">
        <mkdir dir="${classes.dir}"/>
        <javac srcdir="${src.dir}" destdir="${classes.dir}" classpathref="classpath" fork="true" executable="${javac.path}"/>
    </target>

    <target name="pack" depends="compile">
        <jar destfile="${jarfile}" compress="false" duplicate="preserve" index="true">
            <fileset dir="build/classes"/>
            <archives>
                <zips>
                    <fileset dir="lib/" includes="**/*.jar"/>
                </zips>
            </archives>
            <manifest>
                <attribute name="Main-Class" value="Main" />
                <attribute name="Class-Path" value="lib/ classes/" />
                <attribute name="Built-By" value="zonkiie" />
            </manifest>
        </jar>    
    </target>

    <target name="pack_and_run" depends="pack">
        <java fork="true" jar="${jarfile}"/>
    </target>

    <target name="run_package">
        <java fork="true"  jar="${jarfile}"/>
    </target>

    <target name="run" depends="compile">
        <java fork="true" classname="Main" classpathref="classpath">
            <classpath>
                <!--<pathelement location="lib"/> -->
                <pathelement location="${classes.dir}"/>
            </classpath>
        </java>
    </target>

    <target name="native_image" depends="compile">
        <exec dir="." executable="bash">
            <arg value="-c" />
            <arg value="${native_image.path} --verbose -H:+ReportExceptionStackTraces --no-fallback --class-path ${classes.dir}:`find lib/ -type f | paste -s -d ':'` Main javaweb" />
        </exec>
    </target>

    <target name="native_image2" depends="compile">
        <exec dir="." executable="${native_image.path}">
            <arg value="--no-fallback" />
            <arg value="--class-path" />
            <arg value="${lib.dir}:${classes.dir}" />
            <arg value="Main" />
        </exec>
    </target>

</project>
