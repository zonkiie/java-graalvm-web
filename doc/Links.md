# Links for this Project

## Links for GraalVM

## Links for Hibernate
- [https://www.baeldung.com/java-bootstrap-jpa](https://www.baeldung.com/java-bootstrap-jpa)
- [https://www.javaguides.net/2019/11/hibernate-h2-database-example-tutorial.html](https://www.javaguides.net/2019/11/hibernate-h2-database-example-tutorial.html)
- [https://quarkus.io/guides/hibernate-orm](https://quarkus.io/guides/hibernate-orm)

## Links for ant
- [https://stackoverflow.com/questions/42513554/include-multiple-file-extensions-in-ant](https://stackoverflow.com/questions/42513554/include-multiple-file-extensions-in-ant)


