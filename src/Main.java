import java.util.*;
import org.hibernate.*;
import javax.persistence.*;
import util.*;
import entities.*;

public class Main
{
    public static void main(String args[])
    {
        //Session session = HibernateInit.newInstance().createSession();

        //session.close();
        EntityManager entityManager = getJpaEntityManager();
        User u = new User();
        u.firstName = "Hans";
        u.lastName = "Mustermann";
        entityManager.persist(u);
        entityManager.flush();

        Collection<User> userlist = entityManager.createQuery("select u from User u").getResultList();
        for(User singleUser: userlist)
        {
            System.out.println("User:" + singleUser.firstName + " " + singleUser.lastName + " ->" + singleUser);
        }

    }
    private static class EntityManagerHolder {
        private static final EntityManager ENTITY_MANAGER = new JpaEntityManagerFactory(
        new Class[]{User.class, Book.class, Author.class})
        .getEntityManager();
    }

    public static EntityManager getJpaEntityManager() {
        return EntityManagerHolder.ENTITY_MANAGER;
    }

}
