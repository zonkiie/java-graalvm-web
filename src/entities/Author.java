package entities;

import java.lang.*;
import java.util.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.*;

@Entity
@DynamicInsert
@Table(name="author")
public class Author
{
    @Id
    @GeneratedValue
    public UUID id;
    public Date ctime;
    public Date mtime;
    public String firstName;
    public String lastName;
    public String descrption;
    public UUID userId;
}
