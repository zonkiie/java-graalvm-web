package entities;

import java.lang.*;
import java.util.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import org.hibernate.annotations.*;
import javax.persistence.Table;

@Entity
@DynamicInsert
@Table(name="book")
public class Book
{
    @Id
    @GeneratedValue
    public UUID id;
    public Date ctime;
    public Date mtime;
    public String title;
    public String descrption;
    public UUID authorId;
}
