package entities;

import java.lang.*;
import java.util.*;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Entity;
import org.hibernate.annotations.*;
import javax.persistence.Table;

@Entity
@DynamicInsert
@Table(name="user")
public class User
{
    @Id
    @GeneratedValue
    public UUID id;
    public Date ctime;
    public Date mtime;
    public String firstName;
    public String lastName;
    public String password;
    public String descrption;
}
