package util;

import java.lang.*;
import java.util.*;
import java.util.stream.*;
import javax.persistence.*;
import javax.persistence.spi.*;
import org.hibernate.jpa.boot.internal.*;


public class JpaEntityManagerFactory {
    //private String DB_URL = "jdbc:h2:mem:example";
    private String DB_URL = "jdbc:hsqldb:mem:myDb";
    private String DB_USER_NAME = "sa";
    //private String DB_PASSWORD = null;
    private String DB_PASSWORD = "sa";
    private Class[] entityClasses;
    
    public JpaEntityManagerFactory(Class[] entityClasses) {
        this.entityClasses = entityClasses;
    }
    
    public EntityManager getEntityManager() {
        return getEntityManagerFactory().createEntityManager();
    }
    
    protected EntityManagerFactory getEntityManagerFactory() {
        PersistenceUnitInfo persistenceUnitInfo = getPersistenceUnitInfo(
          getClass().getSimpleName());
        Map<String, Object> configuration = new HashMap<>();
        return new EntityManagerFactoryBuilderImpl(
          new PersistenceUnitInfoDescriptor(persistenceUnitInfo), configuration)
          .build();
    }
    
    protected HibernatePersistenceUnitInfo getPersistenceUnitInfo(String name) {
        return new HibernatePersistenceUnitInfo(name, getEntityClassNames(), getProperties());
    }

    protected List<String> getEntityClassNames() {
        return Arrays.asList(getEntities())
          .stream()
          .map(Class::getName)
          .collect(Collectors.toList());
    }
    
    protected Properties getProperties() {
        Properties properties = new Properties();
        properties.put("hibernate.connection.url", DB_URL);
        //properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
        properties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
        properties.put("hibernate.id.new_generator_mappings", false);
        properties.put("hibernate.allow_update_outside_transaction", true);
        properties.put("hibernate.hbm2ddl.auto", "create-drop");
        properties.put("hbm2ddl.auto", "create-drop");
        return properties;
    }
    
    protected Class[] getEntities() {
        return entityClasses;
    }
    
    // additional methods
}

